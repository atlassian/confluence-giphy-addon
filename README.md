# Atlassian Add-on using Express

This Confluence Connect App has been deprecated. It was created years ago and now have vulnerable dependenices that requires updating before being patched. Should you wish to use this app, you can use the latest Atlassian Connect Express to create a modern app and follow the [docs](https://developer.atlassian.com/cloud/confluence/getting-started-with-connect/) to migrate this repo's code over.
## What's next?

[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).
